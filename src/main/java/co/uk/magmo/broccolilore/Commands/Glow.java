package co.uk.magmo.broccolilore.Commands;

import static co.uk.magmo.broccolilore.Utilities.Messenger.playerMessage;

import co.uk.magmo.broccolilore.Utilities.GlowEnchant;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Glow {
    public static void glow(CommandSender sender) {
        Player player = (Player) sender;
        ItemStack itemStack = player.getInventory().getItemInMainHand();
        ItemMeta itemMeta = itemStack.getItemMeta();

        GlowEnchant glow = new GlowEnchant(NamespacedKey.minecraft("53254"));
        itemMeta.addEnchant(glow, 1, true);
        itemStack.setItemMeta(itemMeta);

        playerMessage(player, "Item successfully glowed", false);
    }
}
