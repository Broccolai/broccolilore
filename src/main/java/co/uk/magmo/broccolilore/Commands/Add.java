package co.uk.magmo.broccolilore.Commands;

import static co.uk.magmo.broccolilore.Utilities.Messenger.playerMessage;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Add {

    public static void add(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        ItemStack itemStack = player.getInventory().getItemInMainHand();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (!itemMeta.hasLore()) {
            // Adding new lore

            List<String> lores = new ArrayList<>();
            StringBuilder buildLore = new StringBuilder();

            for (int i = 1; i < args.length; i++)
                buildLore.append(args[i]).append(" ");

            String lore = buildLore.toString();

            lore = lore.replace("&", "§");
            lores.add(lore);

            itemMeta.setLore(lores);
            itemStack.setItemMeta(itemMeta);
        } else {
            // Adding a new line of lore

            List<String> lores = itemMeta.getLore();
            StringBuilder buildLore = new StringBuilder();

            for (int i = 1; i < args.length; i++)
                buildLore.append(args[i]).append(" ");

            String lore = buildLore.toString();

            lore = lore.replace("&", "§");
            lores.add(lore);

            itemMeta.setLore(lores);
            itemStack.setItemMeta(itemMeta);
        }

        playerMessage(player, "Item successfully lored", false);
    }
}
