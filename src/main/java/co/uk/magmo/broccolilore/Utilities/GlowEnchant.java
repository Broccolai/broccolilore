package co.uk.magmo.broccolilore.Utilities;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class GlowEnchant extends Enchantment {
    public GlowEnchant(NamespacedKey id) {
        super(id);
    }

    @Override
    public boolean canEnchantItem(ItemStack arg0) {
        return false;
    }

    @Override
    public boolean conflictsWith(Enchantment arg0) {
        return false;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return null;
    }

    @Override
    public int getMaxLevel() {
        return 0;
    }

    @SuppressWarnings( "deprecation" )
    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getStartLevel() {
        return 0;
    }

    @SuppressWarnings( "deprecation" )
    @Override
    public boolean isCursed() {
        return false;
    }

    @SuppressWarnings( "deprecation" )
    @Override
    public boolean isTreasure() {
        return false;
    }

}
