package co.uk.magmo.broccolilore;

import static co.uk.magmo.broccolilore.Utilities.Messenger.playerMessage;

import co.uk.magmo.broccolilore.Commands.Add;
import co.uk.magmo.broccolilore.Commands.Clear;
import co.uk.magmo.broccolilore.Commands.Glow;
import co.uk.magmo.broccolilore.Commands.Name;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandDispatcher implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commmandlabel, String[] args) {
        Player player = (Player) sender;

        if (args.length == 0) {
            playerMessage(player, "This command is not recognised", true);
            return true;
        }

        if (!player.hasPermission("broccolilore")) {
            playerMessage(player, "You do not have permission", true);
            return true;
        }

        if (player.getInventory().getItemInMainHand() == null) {
            playerMessage(player, "Hold the item in your main hand", true);
            return true;
        }

        switch (args[0].toLowerCase()) {
            case "add":
                Add.add(sender, args);
                return true;

            case "name":
                Name.name(sender, args);
                return true;

            case "clear":
                Clear.clear(sender);
                return true;

            case "clearall":
                Clear.clearAll(sender);
                return true;

            case "glow":
                Glow.glow(sender);
                return true;

            default:
                playerMessage(player, "This command is not recognised", true);
                return true;
        }
    }
}
